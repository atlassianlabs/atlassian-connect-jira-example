# Atlassian Connect app using Express

Congratulations! You've successfully created an Atlassian Connect app using the Express web application framework. This web app greatly simplifies the creation of Atlassian app by simplifying the following:

* Verification of JWT signatures through the use of a custom Express middleware
* JWT signing of outbound HTTP requests back to the host
* Auto registration and deregistration of your app in development mode
* Persistent storage of the host client information (i.e., client key, shared secret and other useful host information)
* Persistent app data storage through a key/value store

## What's next?

* Copy `credentials.json.sample` to `credentials.json` and fill in the blanks to enable auto registration of the app into your JIRA instance.
* `npm install` to install dependencies
* `npm start` to run the app
* For further reading, please [read the docs](https://bitbucket.org/atlassian/atlassian-connect-express/src/master/README.md#markdown-header-install-dependencies).

## Enabling ngrok

After running `npm start` navigate to your tunnel URL and click "Visit site" to enable ngrok.

<details>
<summary>ngrok browser warning</summary>
    <img alt="ngrok browser warning" src="ngrok-browser-warning.png">
</details>

This needs to be done after every time a new tunnel is created, or in other words, every time you run `npm start`

